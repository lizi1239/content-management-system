const path = require('path')/* 获取path，用于合成绝对路径 */

// webpack.config.js
const { VueLoaderPlugin } = require('vue-loader')
const HtmlWebpackPlugin = require("html-webpack-plugin")

module.exports = {

	mode: "development",/* mode可选择none production development		*/
    // entry : './src/main.js', // 入口文件
	 entry: {
	    main: './src/main.js',
	
	  },
    output : {
        filename : 'main.js', // 输出文件名
        path :  path.resolve(__dirname, './dist'), // 输出文件的根路径
		clean: true ,/* output_clean 确定自动生成前是否清除文件夹 */
	 
	},
	devtool:"inline-source-map",/* 开启资源映射 ，将打包后的js文件与源文件映射*/
	devServer: {/* devServer属性是给webpack-dev-server工具添加的 */
		port: 3463,/*  */
	},
	resolve: {
	    alias: {
	        vue: 'vue/dist/vue.js'     ,
			/* vue.js、vue.esm.js等 是完整版，含编译器，可以对template进行编译，可以不用.vue的单组件而是用.js的形式定义组件对象 */
			/* vue.runtime.esm.js等是运行时版，不含编译器，不能对template属性进行编译，如果要使用.js形式的带有template属性的组件形式，就不要使用这个版本 */
			/* .vue形式的组件在使用vue-loader编译后直接产生渲染函数，在生产环境中不用进行编译，最终的代码体积小、速度快 */
			/* .js形式的带有template属性的组件，vue-loader不能对其编译，必须使用带有编译器的vue版本 */
	    }
	},
	

	module: {
		rules: [
	
			{/* asset  */
				test: /\.png$/,
				type: "asset",
				parser: {
					dataUrlCondition: {
						/* 当资源大于1MB，使用URL引入；小于时，直接使用base64内嵌 */
						maxSize: 1*1024*1024 /* 1M*//* 默认为8KB */
					}
				},
				generator: {
					filename: "images/[contenthash][ext]"
				}	
			},
		
		  {
			test: /\.vue$/,
			loader: 'vue-loader'
		  },
		  // 它会应用到普通的 `.js` 文件
		  // 以及 `.vue` 文件中的 `<script>` 块
		  {
			test: /\.js$/,
			loader: 'babel-loader'
		  },
		  // 它会应用到普通的 `.css` 文件
		  // 以及 `.vue` 文件中的 `<style>` 块
		  {
			test: /\.css$/,
			use: [
			  'vue-style-loader',
			  'css-loader'
			]
		  }
		]
  },
  
  plugins: [
    // 请确保引入这个插件来施展魔法
    new VueLoaderPlugin(),
	new HtmlWebpackPlugin({
	      filename: 'index.html', // 打包后的文件名，默认index.html
	      template: path.resolve(__dirname, './src/index.html') // 导入被打包的文件模板
	    })
	
	
  ],
  performance: {/* 解决打包过大问题 */
          hints: "warning", // 枚举
          maxAssetSize: 3000000, // 整数类型（以字节为单位）
          maxEntrypointSize: 5000000, // 整数类型（以字节为单位）
          assetFilter: function (assetFilename) {
              // 提供资源文件名的断言函数
              // 只给出js与css文件的性能提示
              return assetFilename.endsWith('.css') || assetFilename.endsWith('.js');
          }
      },

}